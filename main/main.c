#include "temper.h"

#include "driver/spi_master.h"
#include "esp_spi_flash.h"
#include "esp_system.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#include "radio/ezradio.h"
#include <stdio.h>
#include <string.h>

void app_main() {
    printf("Hello world!\n");

    /* Print chip information */
    esp_chip_info_t chip_info;
    esp_chip_info(&chip_info);
    printf("This is ESP32 chip with %d CPU cores, WiFi%s%s, ",
           chip_info.cores,
           (chip_info.features & CHIP_FEATURE_BT) ? "/BT" : "",
           (chip_info.features & CHIP_FEATURE_BLE) ? "/BLE" : "");

    printf("silicon revision %d, ", chip_info.revision);

    printf("%dMB %s flash\n", spi_flash_get_chip_size() / (1024 * 1024),
           (chip_info.features & CHIP_FEATURE_EMB_FLASH) ? "embedded" : "external");

    esp_err_t ret;

    spi_bus_config_t buscfg = {
            .miso_io_num = 25,
            .mosi_io_num = 23,
            .sclk_io_num = 19,
            .quadwp_io_num = -1,
            .quadhd_io_num = -1,
    };

    // Initialize SPI bus - we must use DMA when communicating with the Si4455
    // because we need to be able to send large amounts of data during EZConfig.
    // Unfortunately that means we also need to manually emulate half-duplex mode.
    ret = spi_bus_initialize(HSPI_HOST, &buscfg, 1);
    ESP_ERROR_CHECK(ret);

    ezradio_config_t config = {
            .host = HSPI_HOST,
            .cs_pin = 21,
            .intr_pin = 16,
            .sdn_pin = 14
    };

    bb_radio_t *radio = NULL;
    ret = ezradio_probe(&config, &radio);
    ESP_ERROR_CHECK(ret);

    radio_tune(radio, 434.5593750);
    printf("CREATED\n");

    fflush(stdout);
}
