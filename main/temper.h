#pragma once

#include "esp_system.h"

#include "driver/spi_master.h"
#include "radio/radio.h"

// The broadcast channel, used for auto-learn functionality
#define TEMPER_BROADCAST_CHANNEL 5568

#define TEMPER_CMD_HEAD_UP          0X96530005
#define TEMPER_CMD_HEAD_DOWN        0X96540005
#define TEMPER_CMD_FLAT             0X965C0400
#define TEMPER_CMD_LEG_UP           0X96510100
#define TEMPER_CMD_LEG_DOWN         0X96520100
#define TEMPER_CMD_MEM_1            0X965C0000
#define TEMPER_CMD_MEM_2            0X965C0100
#define TEMPER_CMD_MEM_3            0X965C0200
#define TEMPER_CMD_MEM_4            0X965C0300
#define TEMPER_CMD_SET_MEM_1        0x965B0000
#define TEMPER_CMD_SET_MEM_2        0x965B0100
#define TEMPER_CMD_SET_MEM_3        0x965B0200
#define TEMPER_CMD_SET_MEM_4        0x965B0300
#define TEMPER_CMD_STOP             0X96860000
#define TEMPER_CMD_MASSAGE_MODE_1   0X968D0078
#define TEMPER_CMD_MASSAGE_MODE_2   0X968D0178
#define TEMPER_CMD_MASSAGE_MODE_3   0X968D0278
#define TEMPER_CMD_MASSAGE_MODE_4   0X968D0378
#define TEMPER_CMD_BROADCAST_CH     0x96000000

#define TEMPER_MASSAGE_MAGIC_MASK   0xFFFF0000
#define TEMPER_MASSAGE_MAGIC_1      0x968E0000
// This one seems to be used when making adjustments to built-in modes
#define TEMPER_MASSAGE_MAGIC_2      0x96850000

#define TEMPER_MASSAGE_TYPE_MASK    0X0000FF00
#define TEMPER_MASSAGE_TYPE_HEAD    0X00000000
#define TEMPER_MASSAGE_TYPE_LUMBAR  0X00000100
#define TEMPER_MASSAGE_TYPE_LEG     0X00000200

#define TEMPER_MASSAGE_LEVEL_MASK   0x000000FF
#define TEMPER_MASSAGE_LEVEL_STEP   0x18
#define TEMPER_MASSAGE_LEVEL_MIN    0    esp_err_t ret;

#define TEMPER_MASSAGE_LEVEL_MAX    10

typedef enum {
    INITIAL = 0,
    RX = 1
} temper_state_t;

typedef struct {
    bb_radio_t* radio;

    volatile temper_state_t state;
    volatile bool handled_reset;

} temper_device_t;

#pragma pack(push, 1)
typedef struct {
    uint32_t cmd;
    uint16_t channel;
    uint8_t crc;
} temper_packet_t;
#pragma pack(pop)

bool temper_cmd_to_str(const temper_packet_t* packet, char* const s, size_t n) __attribute__((warn_unused_result));

esp_err_t temper_create(bb_radio_t* radio, temper_device_t** handle);

//void temper_set_channel(uint16_t channel, temper_device_t const* dev);

// Populate fields of |packet| from the given bytes. Assumes |packet| points to valid memory.
void temper_bytes_to_packet(temper_packet_t* packet, uint8_t bytes[], size_t len);

// Update the |crc| field. Return whether it was correct to begin with.
bool temper_do_crc(temper_packet_t* packet);

// Maps remote control channel # to fc[15:0] ("nominal carrier frequency" from datasheet)
uint16_t temper_channel_to_fc(uint16_t channel);

// Inverse of temper_channel_to_fc
uint16_t temper_fc_to_channel(uint16_t fc);

// Compute crc8 checksum using the system's custom CRC parameters
uint8_t temper_crc(const uint8_t data[], size_t len);
