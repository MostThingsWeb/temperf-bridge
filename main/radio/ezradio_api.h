#ifndef TEMPER_BRIDGE_EZRADIO_API_H
#define TEMPER_BRIDGE_EZRADIO_API_H

#include <stdint.h>
#include <soc/soc.h>

#pragma pack(push, 1)
typedef struct {
    uint8_t boot_options;
    uint8_t xtal_options;
    uint32_t xo_freq;
} ezradio_power_up_args_t;

typedef struct {
    uint8_t cts;
    uint8_t chiprev;
    uint16_t part;
    uint8_t prbuild;
    uint16_t id;
    uint8_t customer;
    uint8_t romid;
} ezradio_part_info_resp_t;

typedef struct {
    uint8_t cts;
    uint8_t revext;
    uint8_t revbranch;
    uint8_t revint;
    uint16_t patch;
    uint8_t func;
} ezradio_func_info_resp_t;

typedef struct {
    uint8_t cts;
    uint8_t rx_fifo_count;
    uint8_t tx_fifo_space;
} ezradio_fifo_info_resp_t;

typedef struct {
    uint8_t cts;

    enum {
        CHIP_INT_PEND = 1 << 2,
        MODEM_INT_PEND = 1 << 1,
        PH_INT_PEND = 1 << 0
    };
    uint8_t int_pend;

    enum {
        CHIP_INT_STATUS = 1 << 2,
        MODEM_INT_STATUS = 1 << 1,
        PH_INT_STATUS = 1 << 0
    };
    uint8_t int_status;

    enum {
        PACKET_SENT_PEND = 1 << 5,
        PACKET_RX_PEND = 1 << 4,
        CRC_ERROR_PEND = 1 << 3,
        ALT_CRC_ERROR_PEND = 1 << 2,
        TX_FIFO_ALMOST_EMPTY_PEND = 1 << 1,
        RX_FIFO_ALMOST_FULL_PEND = 1 << 0
    };
    uint8_t ph_pend;

    enum {
        PACKET_SENT	= 1 << 5,
        PACKET_RX = 1 << 4,
        CRC_ERROR = 1 << 3,
        ALT_CRC_ERROR = 1 << 2,
        TX_FIFO_ALMOST_EMPTY = 1 << 1,
        RX_FIFO_ALMOST_FULL = 1 << 0
    };
    uint8_t ph_status;

    enum {
        RSSI_LATCH_PEND = 1 << 7,
        INVALID_SYNC_PEND = 1 << 5,
        RSSI_PEND = 1 << 3,
        INVALID_PREAMBLE_PEND = 1 << 2,
        PREAMBLE_DETECT_PEND = 1 << 1,
        SYNC_DETECT_PEND = 1 << 0
    };
    uint8_t modem_pend;

    enum {
        RSSI_LATCH = 1 << 7,
        INVALID_SYNC = 1 << 5,
        RSSI = 1 << 3,
        INVALID_PREAMBLE = 1 << 2,
        PREAMBLE_DETECT = 1 << 1,
        SYNC_DETECT = 1 << 0
    };
    uint8_t modem_status;

    enum {
        FIFO_UNDERFLOW_OVERFLOW_ERROR_PEND = 1 << 5,
        STATE_CHANGE_PEND = 1 << 4,
        CMD_ERROR_PEND = 1 << 3,
        CHIP_READY_PEND = 1 << 2
    };
    uint8_t chip_pend;

    enum {
        FIFO_UNDERFLOW_OVERFLOW_ERROR = 1 << 5,
        STATE_CHANGE = 1 << 4,
        CMD_ERROR = 1 << 3,
        CHIP_READY = 1 << 2
    };
    uint8_t chip_status;
} ezradio_get_int_status_resp_t;


typedef struct {
    uint8_t cts;
    uint8_t mode;
    uint8_t current_channel;
} ezradio_request_device_state_resp_t;

typedef struct {
    uint8_t enable_bits;
} ezradio_int_ctl_enable_prop_t;

typedef struct {
    uint8_t enable_bits;
} ezradio_int_ctl_ph_enable_prop_t;

typedef struct {
    uint8_t enable_bits;
} ezradio_int_ctl_modem_enable_prop_t;

typedef struct {
    uint8_t enable_bits;
} ezradio_int_ctl_chip_enable_prop_t;

#pragma pack(pop)

#endif //TEMPER_BRIDGE_EZRADIO_API_H
