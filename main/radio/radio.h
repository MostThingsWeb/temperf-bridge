#ifndef TEMPER_BRIDGE_RADIO_H
#define TEMPER_BRIDGE_RADIO_H

#include <driver/spi_common.h>

#include <freertos/FreeRTOS.h>
#include <freertos/queue.h>

//enum radio_state {
//
//};

typedef struct bb_radio_t bb_radio_t;

struct bb_radio_t {
    void* device;
    QueueHandle_t msg_queue;

    esp_err_t (*tune)(bb_radio_t* radio, float freq);
};

typedef struct {
    uint8_t* data;
    size_t len;
} radio_packet_t;

esp_err_t radio_tune(bb_radio_t* radio, float freq);

esp_err_t radio_send_packet(bb_radio_t* radio, uint8_t *data, size_t len);


QueueHandle_t radio_get_message_queue(bb_radio_t* radio);


// TODO: rename or eliminate need for ezradio to call this
esp_err_t radio_initialize_internal(bb_radio_t *radio);

#endif // TEMPER_BRIDGE_RADIO_H
