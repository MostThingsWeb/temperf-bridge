#include "radio.h"

esp_err_t radio_tune(bb_radio_t* dev, float freq)
{
    if (dev->tune) {
        return dev->tune(dev, freq);
    }

    return ESP_ERR_NOT_SUPPORTED;
}

esp_err_t radio_initialize_internal(bb_radio_t *radio)
{
    radio->msg_queue = xQueueCreate(10, sizeof(unsigned long));
    printf("Queue initialized\n");
    return ESP_OK;
}