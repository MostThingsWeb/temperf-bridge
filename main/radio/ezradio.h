#ifndef TEMPER_BRIDGE_EZRADIO_H
#define TEMPER_BRIDGE_EZRADIO_H

#include "ezradio_api.h"
#include "radio.h"

#include <stdint.h>

#include <driver/spi_common.h>
#include <driver/spi_master.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#define EZRADIO_CMD_PART_INFO                0x01
#define EZRADIO_CMD_REQUEST_DEVICE_STATE     0x33
#define EZRADIO_CMD_READ_CMD_BUFF            0x44
#define EZRADIO_CMD_POWER_UP                 0x02

typedef struct {
    spi_host_device_t host;
    int cs_pin;
    int intr_pin;
    int sdn_pin;
} ezradio_config_t;

typedef struct {
    uint8_t outdiv;
    uint16_t min_freq;
    uint16_t max_freq;
} ezradio_outdiv_entry_t;

typedef struct {
    uint16_t part_id;

    ezradio_outdiv_entry_t const *outdivs;
    const uint8_t * radio_config_data_array;
    bool is_pro;
} ezradio_device_id_t;

typedef struct ezradio_t ezradio_t;

struct ezradio_t {
    ezradio_config_t config;

    spi_device_handle_t spi;
    ezradio_part_info_resp_t part;

    const ezradio_device_id_t* id;

    uint32_t freq_xo;

    xTaskHandle task_handle;
};

esp_err_t ezradio_part_info(ezradio_part_info_resp_t* ret, ezradio_t* dev);

esp_err_t ezradio_probe(const ezradio_config_t *config, bb_radio_t **radioptr);

esp_err_t ezradio_configure(ezradio_t* dev);

esp_err_t ezradio_tune(bb_radio_t* dev, float freq);


#endif //TEMPER_BRIDGE_EZRADIO_H
