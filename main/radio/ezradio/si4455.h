#ifndef TEMPER_BRIDGE_SI4455_H
#define TEMPER_BRIDGE_SI4455_H

#include "../ezradio.h"

extern const ezradio_outdiv_entry_t si4455_outdivs[];
extern const uint8_t SI4455_RADIO_CONFIGURATION_DATA_ARRAY[];

#endif //TEMPER_BRIDGE_SI4455_H
