#include "configs/si4455_config.h"
#include "si4455.h"

#include "../ezradio.h"

const ezradio_outdiv_entry_t si4455_outdivs[] = {
        { .outdiv = 12, .min_freq = 280, .max_freq = 350 },
        { .outdiv = 8,  .min_freq = 425, .max_freq = 525 },
        { .outdiv = 4,  .min_freq = 850, .max_freq = 960 },
        { }
};

const uint8_t SI4455_RADIO_CONFIGURATION_DATA_ARRAY[] = RADIO_CONFIGURATION_DATA_ARRAY;