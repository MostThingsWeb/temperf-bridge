#include "ezradio.h"

#include <driver/spi_master.h>
#include <driver/gpio.h>

#include <freertos/task.h>

#include <string.h>
#include <lwip/def.h>
#include <esp_heap_caps.h>

#include "ezradio/si4455.h"

static void IRAM_ATTR ezradio_isr(void *arg) {
    bb_radio_t *radio = (bb_radio_t *) arg;
    ezradio_t* dev = (ezradio_t*) radio->device;
    gpio_intr_disable(dev->config.intr_pin);
    vTaskNotifyGiveFromISR(dev->task_handle, NULL);
}

esp_err_t ezradio_wait_cts_bounded(spi_device_handle_t spi, int attempts) {
    assert(attempts != 0);

    esp_err_t ret;
    spi_transaction_t trans = {0};

    trans.length = 16;
    trans.tx_data[0] = EZRADIO_CMD_READ_CMD_BUFF;
    trans.tx_data[1] = 0; // don't care
    trans.rxlength = 16;
    trans.flags = SPI_TRANS_USE_RXDATA | SPI_TRANS_USE_TXDATA;

    while (attempts != 0) {
        ret = spi_device_transmit(spi, &trans);
        if (ret != ESP_OK) {
            return ret;
        }

        if (trans.rx_data[1] == 0xff) {
            return ESP_OK;
        }

        vTaskDelay(10 / portTICK_PERIOD_MS);
        if (attempts > 0) {
            attempts--;
        }
    }

    return ESP_ERR_TIMEOUT;
}

esp_err_t ezradio_wait_cts(spi_device_handle_t spi) {
    return ezradio_wait_cts_bounded(spi, -1);
}

esp_err_t _ezradio_send_command(const uint8_t *tx_data, size_t tx_data_bytes, spi_device_handle_t spi) {
    spi_transaction_t trans = {0};
    trans.length = tx_data_bytes * 8;
    trans.tx_buffer = tx_data;

    return spi_device_transmit(spi, &trans);
}

static uint8_t *__attribute__((malloc)) _dma_malloc(size_t size) {
    return heap_caps_malloc((size * 8 + 31) / 8, MALLOC_CAP_DMA | MALLOC_CAP_8BIT);
}

esp_err_t _ezradio_receive_response(uint8_t *resp, size_t resp_bytes, spi_device_handle_t spi) {
    esp_err_t ret;
    spi_transaction_t trans = {0};
    uint8_t *rxbuffer = NULL;

    // Conceptually we are just sending a single byte (READ_CMD_BUFF), but since we're in
    // full-duplex mode, we need to pad out the tx buffer to be the same length as the response
    // we are expecting to read. Just fill it with 0.
    uint8_t *txbuffer = _dma_malloc(resp_bytes + 1);
    if (txbuffer == NULL) {
        ret = ESP_ERR_NO_MEM;
        goto out;
    }

    memset(txbuffer, 0, resp_bytes + 1);
    txbuffer[0] = EZRADIO_CMD_READ_CMD_BUFF;

    trans.tx_buffer = txbuffer;
    trans.length = (resp_bytes + 1) * 8;

    // As with the tx stage, we need to pad the rx buffer an extra byte to account for the command byte.
    rxbuffer = _dma_malloc(resp_bytes + 1);
    if (rxbuffer == NULL) {
        ret = ESP_ERR_NO_MEM;
        goto out;
    }
    trans.rxlength = trans.length;
    trans.rx_buffer = rxbuffer;

    ret = spi_device_transmit(spi, &trans);
    if (ret != ESP_OK) {
        goto out;
    }

    assert(rxbuffer[1] == 0xff);
    memcpy(resp, rxbuffer + 1, resp_bytes);
out:
    heap_caps_free(txbuffer);
    heap_caps_free(rxbuffer);
    return ret;
}

esp_err_t ezradio_send_receive(const uint8_t *tx_data, size_t tx_data_bytes, uint8_t *resp, size_t resp_bytes,
                               spi_device_handle_t spi) {
    esp_err_t ret = ezradio_wait_cts(spi);
    if (ret != ESP_OK) {
        return ret;
    }

    // TODO: if this fails, send some NOPs or something to get the radio back in sync?
    ret = _ezradio_send_command(tx_data, tx_data_bytes, spi);
    if (ret != ESP_OK) {
        return ret;
    }

    ret = ezradio_wait_cts(spi);
    if (ret != ESP_OK) {
        return ret;
    }

    ret = _ezradio_receive_response(resp, resp_bytes, spi);
    return ret;
}

esp_err_t _ezradio_execute_command(uint8_t command, const uint8_t *args, size_t arg_bytes, uint8_t *data,
                                   size_t data_bytes,
                                   spi_device_handle_t spi) {
    uint8_t *tx_data = _dma_malloc(arg_bytes + 1);
    if (tx_data == NULL) {
        return ESP_ERR_NO_MEM;
    }

    tx_data[0] = command;
    if (arg_bytes > 0) {
        memcpy(tx_data + 1, args, arg_bytes);
    }

    esp_err_t ret = ezradio_send_receive(tx_data, arg_bytes + 1, data, data_bytes, spi);
    heap_caps_free(tx_data);
    return ret;
}
//
//esp_err_t _ezradio_get_property(uint8_t group, uint8_t property, uint8_t *data, spi_device_handle_t spi) {
//    uint8_t args[1] = {0};
//    return _ezradio_execute_command(0x15, args, 1, data, 3, spi);
//}

esp_err_t _ezradio_part_info(ezradio_part_info_resp_t *resp, spi_device_handle_t spi) {
    assert(sizeof(ezradio_part_info_resp_t) == 9);
    esp_err_t ret = _ezradio_execute_command(EZRADIO_CMD_PART_INFO, NULL, 0, (uint8_t *) resp,
                                             sizeof(ezradio_part_info_resp_t), spi);
    if (ret != ESP_OK) {
        return ret;
    }

    resp->part = ntohs(resp->part);
    resp->id = ntohs(resp->id);
    return ESP_OK;
}

esp_err_t ezradio_part_info(ezradio_part_info_resp_t *resp, ezradio_t *dev) {
    return _ezradio_part_info(resp, dev->spi);
}

esp_err_t ezradio_configure(ezradio_t *dev) {
    const uint8_t *data = dev->id->radio_config_data_array;

    while (*data != 0) {
        size_t size_bytes = *data++;

        uint8_t command[size_bytes];
        memcpy(command, data, size_bytes);
        printf("Processing command %x with # bytes: %d\n", command[0], size_bytes);

        if (command[0] == EZRADIO_CMD_POWER_UP) {
            assert(size_bytes == 7);
            memcpy(&dev->freq_xo, command + 3, 4);
            dev->freq_xo = ntohl(dev->freq_xo);
        }

        data += size_bytes;
        uint8_t resp[1] = {0};
        for (size_t p = 0; p < size_bytes; p++) {
            printf("\tbyte %d  = %x\n", p, command[p]);
        }
        ezradio_send_receive(command, size_bytes, resp, 1, dev->spi);
    }

    return ESP_OK;
}

uint8_t ezradio_lookup_outdiv(ezradio_t *dev, float freq) {
    for (ezradio_outdiv_entry_t const *entry = dev->id->outdivs; entry->outdiv != 0; entry++) {
        if (entry->min_freq <= freq && entry->max_freq >= freq) {
            return entry->outdiv;
        }
    }

    return 0;
}

#include <math.h>

esp_err_t ezradio_tune(bb_radio_t *radio, float freq) {
    ezradio_t *dev = (ezradio_t *) radio->device;
    printf("ezradio_tune: OK, you got the device: %f\n", freq);
    uint32_t freq_xo = dev->freq_xo;
    assert(freq_xo != 0);
    printf("freq_xo = %x\n", freq_xo);
    uint8_t n_presc = 4;

    uint8_t outdiv = ezradio_lookup_outdiv(dev, freq);
    printf("lookup outdiv: %d\n", outdiv);

    freq = freq * pow10f(6);

    float N = freq / ((n_presc * (float) freq_xo) / outdiv);

    float integ;
    float frac = modff(N, &integ);

    integ--;
    frac++;

    assert(frac >= 1 && frac <= 2);

    printf("N = %f, integ = %f, frac = %f\n", N, integ, frac);

    uint32_t fc_frac = (uint32_t) (frac * powf(2, 19));
    printf("fc_frac = %x\n", fc_frac);

    uint8_t tx[] = {0x11, 0x40, 4, 0x0, (uint8_t) integ, fc_frac >> 16, (fc_frac & 0xFF00) >> 8, fc_frac & 0xFF};
    printf("TX: %x %x %x %x\n", tx[4], tx[5], tx[6], tx[7]);
    uint8_t rx[] = {0};
    ezradio_send_receive(tx, sizeof(tx), rx, sizeof(rx), dev->spi);

    printf("Attempting to clear interrupts\n");
    for (int i = 0; i < 20; i++) {
        ezradio_get_int_status_resp_t r;
        _ezradio_execute_command(0x20, NULL, 0, (uint8_t *) &r, sizeof(r), dev->spi);
        printf("%x %x %x %x %x %x %x %x %x\n", r.cts, r.int_pend & CHIP_INT_PEND, r.int_status, r.ph_pend, r.ph_status, r.modem_pend,
               r.modem_status, r.chip_pend, r.chip_status);
        //printf("%x %x %x %x %x %x %x %x %x \n", r[0], r[1], r[2], r[3], r[4] , r[5], r[6], r[7], r[8]);
    }

    vTaskDelay(25 / portTICK_PERIOD_MS);

    uint8_t r[3] = {0};
    _ezradio_execute_command(0x33, NULL, 0, r, sizeof(r), dev->spi);
    printf("Device state: %x %x %x\n", r[0], r[1], r[2]);

    uint8_t start_args[] = {0, 0, 0, 0, 0x8, 0x8, 0x8};
    uint8_t start_ret[1];
    _ezradio_execute_command(0x32, start_args, sizeof(start_args), start_ret, sizeof(start_ret), dev->spi);

    _ezradio_execute_command(0x33, NULL, 0, r, sizeof(r), dev->spi);
    printf("Device state: %x %x %x\n", r[0], r[1], r[2]);

    printf("enabling interrupt: %d\n", dev->config.intr_pin);
    gpio_intr_enable(dev->config.intr_pin);

    return ESP_OK;
}

static const ezradio_device_id_t ezradio_devices_match[] = {
        {
                .part_id = 0x4455,
                .outdivs = si4455_outdivs,
                .radio_config_data_array = SI4455_RADIO_CONFIGURATION_DATA_ARRAY,
                .is_pro = false
        },
        {}
};

const ezradio_device_id_t *ezradio_find_device_match(uint16_t part) {
    for (const ezradio_device_id_t *id = ezradio_devices_match; id->part_id != 0; id++) {
        if (id->part_id == part) {
            return id;
        }
    }

    return NULL;
}

unsigned long p = 10;

static void ezradio_fifo_task(void *arg) {
    bb_radio_t *radio = (bb_radio_t *) arg;
    ezradio_t *dev = (ezradio_t *) radio->device;
    printf("ezradio_fifo_task setup\n");
    while (1) {
        ulTaskNotifyTake(true, portMAX_DELAY);
        printf("ezradio_fifo_task: got interrupt!\n");

        uint8_t args[3] = {0, 0, 0};
        ezradio_get_int_status_resp_t r;
        _ezradio_execute_command(0x20, args, 3, (uint8_t *) &r, sizeof(r), dev->spi);
        printf("Interrupts: 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x 0x%02x\n", r.cts, r.int_pend, r.int_status, r.ph_pend, r.ph_status,
               r.modem_pend, r.modem_status, r.chip_pend, r.chip_status);

        printf("\tint_pend: ");
        if (r.int_pend & CHIP_INT_PEND) printf("CHIP ");
        if (r.int_pend & MODEM_INT_PEND) printf("MODEM ");
        if (r.int_pend & PH_INT_PEND) printf("PH");
        printf("\n");

        printf("\tmodem pend: ");
        if (r.modem_pend & INVALID_SYNC_PEND) printf("INVALID_SYNC_PEND ");
        if (r.modem_pend & RSSI_PEND) printf("RSSI_PEND ");
        if (r.modem_pend & INVALID_PREAMBLE_PEND) printf("INVALID_PREAMBLE_PEND ");
        if (r.modem_pend & PREAMBLE_DETECT_PEND) printf("PREAMBLE_DETECT_PEND ");
        if (r.modem_pend & SYNC_DETECT_PEND) printf("SYNC_DETECT_PEND ");
        printf("\n");

        printf("\tph pend: ");
        if (r.ph_pend & PACKET_RX_PEND) printf("PACKET_RX_PEND ");
        if (r.ph_pend & CRC_ERROR_PEND) printf("CRC_ERROR_PEND ");
        if (r.ph_pend & TX_FIFO_ALMOST_EMPTY_PEND) printf("TX_FIFO_ALMOST_EMPTY_PEND ");
        if (r.ph_pend & RX_FIFO_ALMOST_FULL_PEND) printf("RX_FIFO_ALMOST_FULL_PEND ");
        printf("\n");

        uint8_t r2[3] = {0};
        _ezradio_execute_command(0x33, NULL, 0, r2, sizeof(r2), dev->spi);
        printf("Device state: %x %x %x\n", r2[0], r2[1], r2[2]);

        if (xQueueSend(radio->msg_queue, &p, 100/portTICK_PERIOD_MS  ) != pdTRUE) {
            printf("FAIL\n");
        }

        gpio_intr_enable(dev->config.intr_pin);
    }

//    for (;;) {
//        switch (dev->state) {
//        case INITIAL: {
//           //temper_set_channel(6070, dev);
//
//
////            si443x_write(0x8, 0x02, dev->spi_dev);
////            vTaskDelay(25 / portTICK_PERIOD_MS);
////            si443x_write(0x8, 0x10, dev->spi_dev);
//
//            printf("Attempting to clear interrupts\n");
//            for (int i = 0; i < 20; i++) {
//                uint8_t a[] = {0xff, 0xff, 0xff};
//                uint8_t r[9];
//                si446x_execute_command(0x20, NULL, 0, r, sizeof(r), dev->spi_dev);
//                printf("%x %x %x %x %x %x %x %x %x \n", r[0], r[1], r[2], r[3], r[4] , r[5], r[6], r[7], r[8]);
//            }
//
//            vTaskDelay(25 / portTICK_PERIOD_MS);
//
//            uint8_t r[3] = {0};
//            si446x_execute_command(0x33, NULL, 0, r, sizeof(r), dev->spi_dev);
//
//            printf("Device state: %x %x %x\n", r[0], r[1], r[2]);
//
//            uint8_t start_args[] = {0, 0, 0, 0, 0, 0, 0};
//            uint8_t start_ret[1];
//            si446x_execute_command(0x32, start_args, sizeof(start_args), start_ret, sizeof(start_ret), dev->spi_dev);
//
//            printf("enabling interrupt: %d\n", dev->config.intr_pin);
//            gpio_intr_enable(dev->config.intr_pin);
//
//            dev->state = RX;
//
//            break;
//        }
//        case RX: {
//            int64_t t = 0;
//            printf("Waiting for packets...\n");
//            while (true) {
//                ulTaskNotifyTake(true, portMAX_DELAY);
//                printf("GOT INTERRUPT!\n");
//                uint8_t a[] = {0, 0, 0};
//                uint8_t r[9];
//                si446x_execute_command(0x20, a, sizeof(a), r, sizeof(r), dev->spi_dev);
//
//                printf("%x %x %x %x %x %x %x %x %x \n", r[0], r[1], r[2], r[3], r[4], r[5], r[6], r[7], r[8]);
//                if (r[5] & 0x2) {
//                    printf("got preamble;");
//                }
//                if (r[5] & 0x1) {
//                    printf("Got sync\n");
//                }
////                    if (r[5] & 0x4) {
////                        printf("Got bad preamble\n");
////                    }
//                if (r[5] & (1<<5)) {
//                    printf("Got bad sync - restarting RX mode");
//                    uint8_t start_args[] = {0, 0, 0, 0, 0, 0, 0};
//                    uint8_t start_ret[1];
//                    si446x_execute_command(0x32, start_args, sizeof(start_args), start_ret, sizeof(start_ret), dev->spi_dev);
//                }
////
////                vTaskDelay(250 / portTICK_PERIOD_MS);
////                uint8_t r2[3] = {0};
////                si446x_execute_command(0x33, NULL, 0, r2, sizeof(r2), dev->spi_dev);
////
////                printf("Device state: %x %x %x\n", r2[0], r2[1], r2[2]);
//                //si446x_execute_command(0x21, NULL, 0, a, sizeof(a), dev->spi_dev);
//                //byte = si443x_read(SI443X_REG_ISR_STATUS_1, dev->spi_dev);
////                if (byte & 0x2) {
////                    printf("got packet. %d\n", ++cnt);
////                    byte = si443x_read(SI443X_REG_RECV_PACKET_LEN, dev->spi_dev);
////                    printf("len = %d\n", byte);
////                    uint8_t d[8] = { 0 };
////                    si443x_transmit_burst(false, SI443X_REG_FIFO, d, 8, dev->spi_dev);
////
////                    temper_packet_t cmd = { 0 };
////                    temper_bytes_to_packet(&cmd, d + 1, 7);
////
////                    char cmd_str[32];
////                    if (temper_cmd_to_str(&cmd, cmd_str, sizeof(cmd_str))) {
////                        int64_t new_t = esp_timer_get_time();
////                        if (t != 0) {
////                            int64_t diff = new_t - t;
////                            printf("%" PRId64, diff);
////                        }
////                        t = new_t;
////                        if (!temper_do_crc(&cmd)) {
////                            printf("[ %s ]\n", cmd_str);
////                        } else {
////                            printf("[ %s ] - BAD CRC!\n", cmd_str);
////                        }
////                    } else {
////                        printf("0x%08X", cmd.cmd);
////                        printf("0x%04X", cmd.channel);
////                    }
////                }
////                byte = si443x_read(SI443X_REG_ISR_STATUS_2, dev->spi_dev);
//            }
//            break;
//        }
//        }
//    }
}

esp_err_t ezradio_probe(const ezradio_config_t *const config, bb_radio_t **radioptr) {
    int ret;

    ezradio_t *dev = calloc(1, sizeof(ezradio_t));
    if (dev == NULL) {
        ret = ESP_ERR_NO_MEM;
        *radioptr = NULL;
        goto err1;
    }

    *radioptr = calloc(1, sizeof(bb_radio_t));
    if (*radioptr == NULL) {
        ret = ESP_ERR_NO_MEM;
        goto err1;
    }

    ret = radio_initialize_internal(*radioptr);
    if (ret != ESP_OK) {
        goto err1;
    }

    (*radioptr)->device = dev;
    (*radioptr)->tune = ezradio_tune;

    memcpy(&dev->config, config, sizeof(ezradio_config_t));

    // Create the SPI device
    spi_device_interface_config_t spi_dev_cfg = {
            .clock_speed_hz = 80 * 100,
            .mode = 0,
            .spics_io_num = config->cs_pin,
            .queue_size = 7, // TODO: A smaller queue size?
    };

    ret = spi_bus_add_device(config->host, &spi_dev_cfg, &dev->spi);
    if (ret != ESP_OK) {
        goto err1;
    }

    gpio_config_t sdn_config;
    sdn_config.mode = GPIO_MODE_OUTPUT;
    sdn_config.pull_down_en = 0;
    sdn_config.pull_up_en = 0;
    sdn_config.pin_bit_mask = 1 << config->sdn_pin;
    gpio_config(&sdn_config);

    gpio_set_level(config->sdn_pin, 1);
    vTaskDelay(10 / portTICK_PERIOD_MS);
    gpio_set_level(config->sdn_pin, 0);
    vTaskDelay(10 / portTICK_PERIOD_MS);

    // See if there's a device on the bus
    ret = ezradio_wait_cts_bounded(dev->spi, 10);
    if (ret != ESP_OK) {
        if (ret == ESP_ERR_TIMEOUT) {
            printf("Timeout while probing bus\n");
        } else {
            printf("Failure...\n");
        }

        goto err2;
    }

    vTaskDelay(10 / portTICK_PERIOD_MS);
    printf("Found the device!\n");

    // Now identify the particular part
    ezradio_part_info_resp_t part_info;
    ret = _ezradio_part_info(&part_info, dev->spi);
    if (ret != ESP_OK) {
        goto err2;
    }

    memcpy(&dev->part, &part_info, sizeof(ezradio_part_info_resp_t));

    printf("Part: %x\n", part_info.part);

    dev->id = ezradio_find_device_match(part_info.part);
    if (dev->id == NULL) {
        printf("Not supported!\n");
        ret = ESP_ERR_NOT_SUPPORTED;
        goto err2;
    }

    ESP_ERROR_CHECK(ezradio_configure(dev));

    printf("FREQ XO = %x\n", dev->freq_xo);

    // Configure interrupt pin
    gpio_config_t intr_config;
    intr_config.intr_type = GPIO_INTR_LOW_LEVEL;
    //intr_config.intr_type = GPIO_INTR_NEGEDGE;
    intr_config.mode = GPIO_MODE_INPUT;
    intr_config.pull_down_en = 0;
    // According to the datasheet it's not strictly necessary to pull up the pin, but do it anyway
    intr_config.pull_up_en = 1;
    intr_config.pin_bit_mask = 1 << config->intr_pin;
    ret = gpio_config(&intr_config);
    ESP_ERROR_CHECK(ret);
    if (ret != ESP_OK) {
        goto err2;
    }

    ret = gpio_intr_disable(config->intr_pin);
    ESP_ERROR_CHECK(ret);

    // Start the task
    int tc_ret = xTaskCreate(ezradio_fifo_task, "ezradio_fifo_task", 2048,
                             (void *) *radioptr, 10, &dev->task_handle);
    assert(tc_ret == pdPASS);

    ret = gpio_install_isr_service(0);
    ESP_ERROR_CHECK(ret); // TODO
    if (ret != ESP_OK && ret != ESP_ERR_INVALID_STATE) {
        ESP_ERROR_CHECK(ESP_ERR_NO_MEM);
        goto err2;
    }

    ret = gpio_isr_handler_add(config->intr_pin, ezradio_isr, (void *) *radioptr);
    ESP_ERROR_CHECK(ret); // TODO

    goto out;

err2:
    spi_bus_remove_device(dev->spi);
err1:
    free(dev);
    free(*radioptr);
out:
    return ret;
}
