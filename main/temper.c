#include "temper.h"

#include "esp_system.h"
#include "lwip/def.h"

#include "driver/gpio.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#include <inttypes.h>
#include <string.h>

#define SI443X_CLK_HZ 80 * 100

static portMUX_TYPE isr_mutex = portMUX_INITIALIZER_UNLOCKED;
static xTaskHandle temper_task_handle = NULL;

static void IRAM_ATTR temper_isr_handler(void *arg)
{
    temper_device_t *dev = (temper_device_t *)arg;
//
//    taskENTER_CRITICAL_ISR(&isr_mutex);
//    switch (dev->state) {
//    case INITIAL:
//        if (!dev->handled_reset) {
//            dev->handled_reset = true;
//            gpio_intr_disable(dev->config.intr_pin);
//            vTaskNotifyGiveFromISR(temper_task_handle, NULL);
//        }
//        break;
//    case RX:
//        vTaskNotifyGiveFromISR(temper_task_handle, NULL);
//        break;
//    }
    taskEXIT_CRITICAL_ISR(&isr_mutex);
}

void temper_bytes_to_packet(temper_packet_t *packet, uint8_t bytes[],
    size_t len)
{
    assert(packet != NULL);
    assert(len >= 7);
    // Command data in network byte order
    temper_packet_t n_cmd;
    memcpy(&n_cmd, bytes, 7);
    packet->cmd = ntohl(n_cmd.cmd);
    packet->channel = ntohs(n_cmd.channel);
    packet->crc = n_cmd.crc;
}

bool temper_do_crc(temper_packet_t *packet)
{
    uint8_t crc = temper_crc((uint8_t *)&packet, 6);
    bool ret = crc == packet->crc;
    packet->crc = ret;
    return ret;
}

esp_err_t temper_create(bb_radio_t* radio,
    temper_device_t **const handle)
{
//    esp_err_t ret;
//    // Allocate device struct
//    temper_device_t *dev = calloc(1, sizeof(temper_device_t));
//    if (dev == NULL) {
//        return ESP_ERR_NO_MEM;
//    }
//
//    *handle = dev;
//    dev->radio = radio;
//
//    // Start with interrupt disabled - it will be enabled right before resetting
//    // the chip
//    ret = gpio_intr_disable(config->intr_pin);
//    ESP_ERROR_CHECK(ret);
//
//    // Start the task, pinned to PRO_CPU
//    int fr_ret = xTaskCreatePinnedToCore(temper_task, "temper_task", 2048,
//        (void *)dev, 10, &temper_task_handle, 0);
//    assert(fr_ret == pdPASS);
//
//    gpio_install_isr_service(0);
//    gpio_isr_handler_add(config->intr_pin, temper_isr_handler, (void *)dev);
//
    return ESP_OK;
}

uint16_t temper_channel_to_fc(uint16_t channel)
{
    if (channel > 8862) {
        return 2 * channel + 10658;
    }

    return channel + 19520;
}

uint16_t temper_fc_to_channel(uint16_t fc)
{
    if (fc > 28382) {
        return (fc - 10658) / 2;
    }

    return fc - 19520;
}

// CRC table generated from http://www.sunshine2k.de/coding/javascript/crc/crc_js.html
// The parameters were reversed using http://reveng.sourceforge.net/
// Poly: 0x8D, initial: 0xFF
const DRAM_ATTR uint8_t temper_crc_table[] = {
    0x00, 0x8D, 0x97, 0x1A, 0xA3, 0x2E, 0x34, 0xB9, 0xCB, 0x46, 0x5C, 0xD1, 0x68, 0xE5, 0xFF, 0x72,
    0x1B, 0x96, 0x8C, 0x01, 0xB8, 0x35, 0x2F, 0xA2, 0xD0, 0x5D, 0x47, 0xCA, 0x73, 0xFE, 0xE4, 0x69,
    0x36, 0xBB, 0xA1, 0x2C, 0x95, 0x18, 0x02, 0x8F, 0xFD, 0x70, 0x6A, 0xE7, 0x5E, 0xD3, 0xC9, 0x44,
    0x2D, 0xA0, 0xBA, 0x37, 0x8E, 0x03, 0x19, 0x94, 0xE6, 0x6B, 0x71, 0xFC, 0x45, 0xC8, 0xD2, 0x5F,
    0x6C, 0xE1, 0xFB, 0x76, 0xCF, 0x42, 0x58, 0xD5, 0xA7, 0x2A, 0x30, 0xBD, 0x04, 0x89, 0x93, 0x1E,
    0x77, 0xFA, 0xE0, 0x6D, 0xD4, 0x59, 0x43, 0xCE, 0xBC, 0x31, 0x2B, 0xA6, 0x1F, 0x92, 0x88, 0x05,
    0x5A, 0xD7, 0xCD, 0x40, 0xF9, 0x74, 0x6E, 0xE3, 0x91, 0x1C, 0x06, 0x8B, 0x32, 0xBF, 0xA5, 0x28,
    0x41, 0xCC, 0xD6, 0x5B, 0xE2, 0x6F, 0x75, 0xF8, 0x8A, 0x07, 0x1D, 0x90, 0x29, 0xA4, 0xBE, 0x33,
    0xD8, 0x55, 0x4F, 0xC2, 0x7B, 0xF6, 0xEC, 0x61, 0x13, 0x9E, 0x84, 0x09, 0xB0, 0x3D, 0x27, 0xAA,
    0xC3, 0x4E, 0x54, 0xD9, 0x60, 0xED, 0xF7, 0x7A, 0x08, 0x85, 0x9F, 0x12, 0xAB, 0x26, 0x3C, 0xB1,
    0xEE, 0x63, 0x79, 0xF4, 0x4D, 0xC0, 0xDA, 0x57, 0x25, 0xA8, 0xB2, 0x3F, 0x86, 0x0B, 0x11, 0x9C,
    0xF5, 0x78, 0x62, 0xEF, 0x56, 0xDB, 0xC1, 0x4C, 0x3E, 0xB3, 0xA9, 0x24, 0x9D, 0x10, 0x0A, 0x87,
    0xB4, 0x39, 0x23, 0xAE, 0x17, 0x9A, 0x80, 0x0D, 0x7F, 0xF2, 0xE8, 0x65, 0xDC, 0x51, 0x4B, 0xC6,
    0xAF, 0x22, 0x38, 0xB5, 0x0C, 0x81, 0x9B, 0x16, 0x64, 0xE9, 0xF3, 0x7E, 0xC7, 0x4A, 0x50, 0xDD,
    0x82, 0x0F, 0x15, 0x98, 0x21, 0xAC, 0xB6, 0x3B, 0x49, 0xC4, 0xDE, 0x53, 0xEA, 0x67, 0x7D, 0xF0,
    0x99, 0x14, 0x0E, 0x83, 0x3A, 0xB7, 0xAD, 0x20, 0x52, 0xDF, 0xC5, 0x48, 0xF1, 0x7C, 0x66, 0xEB,
};

uint8_t temper_crc(const uint8_t data[], size_t len)
{
    // Compute CRC using table lookup method
    uint8_t ret = 0xFF;
    for (size_t i = 0; i < len; i++) {
        uint8_t byte = data[i] ^ ret;
        ret = temper_crc_table[byte];
    }

    return ret;
}

bool temper_cmd_to_str(const temper_packet_t *const packet, char *const s,
    size_t n)
{
    assert(s);

    switch (packet->cmd) {
    case TEMPER_CMD_HEAD_UP:
        strncpy(s, "Head up", n);
        break;
    case TEMPER_CMD_HEAD_DOWN:
        strncpy(s, "Head down", n);
        break;
    case TEMPER_CMD_FLAT:
        strncpy(s, "Flat", n);
        break;
    case TEMPER_CMD_LEG_UP:
        strncpy(s, "Legs up", n);
        break;
    case TEMPER_CMD_LEG_DOWN:
        strncpy(s, "Legs down", n);
        break;
    case TEMPER_CMD_MEM_1:
        strncpy(s, "Memory 1", n);
        break;
    case TEMPER_CMD_MEM_2:
        strncpy(s, "Memory 2", n);
        break;
    case TEMPER_CMD_MEM_3:
        strncpy(s, "Memory 3", n);
        break;
    case TEMPER_CMD_MEM_4:
        strncpy(s, "Memory 4", n);
        break;
    case TEMPER_CMD_SET_MEM_1:
        strncpy(s, "Set memory 1", n);
        break;
    case TEMPER_CMD_SET_MEM_2:
        strncpy(s, "Set memory 2", n);
        break;
    case TEMPER_CMD_SET_MEM_3:
        strncpy(s, "Set memory 3", n);
        break;
    case TEMPER_CMD_SET_MEM_4:
        strncpy(s, "Set memory 4", n);
        break;
    case TEMPER_CMD_STOP:
        strncpy(s, "Stop", n);
        break;
    case TEMPER_CMD_MASSAGE_MODE_1:
        strncpy(s, "Massage mode 1", n);
        break;
    case TEMPER_CMD_MASSAGE_MODE_2:
        strncpy(s, "Massage mode 2", n);
        break;
    case TEMPER_CMD_MASSAGE_MODE_3:
        strncpy(s, "Massage mode 3", n);
        break;
    case TEMPER_CMD_MASSAGE_MODE_4:
        strncpy(s, "Massage mode 4", n);
        break;
    case TEMPER_CMD_BROADCAST_CH: {
        snprintf(s, n, "Auto-learn broadcast, ch. %d", packet->channel);
        break;
    }
    default: {
        if ((packet->cmd & TEMPER_MASSAGE_MAGIC_MASK) != TEMPER_MASSAGE_MAGIC_1 && (packet->cmd & TEMPER_MASSAGE_MAGIC_MASK) != TEMPER_MASSAGE_MAGIC_2) {
            return false;
        }

        const char *massage_type = NULL;
        switch (packet->cmd & TEMPER_MASSAGE_TYPE_MASK) {
        case TEMPER_MASSAGE_TYPE_HEAD:
            massage_type = "head";
            break;
        case TEMPER_MASSAGE_TYPE_LUMBAR:
            massage_type = "lumbar";
            break;
        case TEMPER_MASSAGE_TYPE_LEG:
            massage_type = "legs";
            break;
        default:
            return false;
        }

        uint8_t level = (packet->cmd & TEMPER_MASSAGE_LEVEL_MASK) / TEMPER_MASSAGE_LEVEL_STEP;
        snprintf(s, n, "Massage %s level %d", massage_type, level);
    }
    }

    s[n - 1] = '\0';

    return true;
}
